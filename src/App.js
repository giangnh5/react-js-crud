import MainContent from './components/Content';

function App() {
  return (
    <div >
      <MainContent />
    </div>
  );
}

export default App;
