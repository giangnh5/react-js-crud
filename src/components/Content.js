import { useEffect, useState } from "react"
import SnackBar from "./notification/SnackBar";
import CreateModal from "./modal/CreateModal";
import DeleteModal from "./modal/DeleteModal";
import EditModal from "./modal/EditModal";
import PaginationTable from "./pagination/PaginationTable";


import Tables from "./table/Tables"

const MainContent = () => {

    const [arrTable, setArrTable] = useState([]);

    const [arrDrink, setArrDrink] = useState([])

    const [page, setPage] = useState(1)

    const [noPage, setNoPage] = useState(0)

    const limit = 10

    
    const [openCreateModal, setOpenCreateModal] = useState(false)
  
    const [openEditModal, setOpenEditModal] = useState(false)
    
    const [openDeleteModal, setOpenDeleteModal] = useState(false)


    // Hiện Alert
    const [alert, setAlert] = useState({
        status: false,
        text: '',
        severity: null
    })

    // State chứa object create 
    const [objectCreate, setObjectCreate] = useState({
        kichCo: '',
        duongKinh: '',
        suon: '',
        salad: '',
        loaiPizza: '',
        idVourcher: '',
        idLoaiNuocUong: '',
        soLuongNuoc: '',
        hoTen: '',
        thanhTien: '',
        email: '',
        soDienThoai: '',
        diaChi: '',
        loiNhan: ''
    })

    // State nhận data khi click row đó
    const [dataRow, setDataRow] = useState({
        kichCo: null,
        duongKinh: null,
        suon: null,
        salad: null,
        loaiPizza: null,
        idVourcher: null,
        idLoaiNuocUong: null,
        soLuongNuoc: null,
        hoTen: null,
        thanhTien: null,
        email: null,
        soDienThoai: null,
        diaChi: null,
        loiNhan: null
    })

    const [deleteId, setDeleteId] = useState('')


    // Tạo promise xử lí tác vụ bất đồng bộ!
    const fetchApi = async (url, body) => {
        const response = await fetch(url, body);
        const data = await response.json();
        return data
    }

    // Get All Order
    useEffect(() => {
        // Get All Order
        fetchApi('http://42.115.221.44:8080/devcamp-pizza365/orders')
            .then((data) => {
                setArrTable(data.slice(limit * (page - 1), limit * page));
                setNoPage(Math.ceil(data.length / limit)); 

            })
    }, [page, noPage, limit, openCreateModal, openEditModal, openDeleteModal]) // Khi false thi các data đc update lại

    //Get Drink
    useEffect(() => {
        fetchApi('http://42.115.221.44:8080/devcamp-pizza365/drinks')
            .then((data) => {
                setArrDrink(data)
            })
    }, [])

    return (
        <>
            <Tables
                arrTable={arrTable} 
                setOpenCreateModal={setOpenCreateModal}
                setOpenEditModal={setOpenEditModal} 
                setOpenDeleteModal={setOpenDeleteModal} 
                setDeleteId={setDeleteId} 
                setDataRow={setDataRow} 
            />
            <PaginationTable
                noPage={noPage}
                page={page}
                setPage={setPage}
            />

            <CreateModal
                openCreateModal={openCreateModal}
                setOpenCreateModal={setOpenCreateModal}
                arrDrink={arrDrink} 
                objectCreate={objectCreate}
                setObjectCreate={setObjectCreate} 
                setAlert={setAlert}
            />

            <EditModal
                openEditModal={openEditModal}
                setOpenEditModal={setOpenEditModal}
                dataRow={dataRow}
                setDataRow={setDataRow}
                arrDrink={arrDrink}
                setAlert={setAlert}
            />

            <DeleteModal
                openDeleteModal={openDeleteModal}
                setOpenDeleteModal={setOpenDeleteModal}
                deleteId={deleteId}
                setAlert={setAlert}
            />

            <SnackBar
                alert={alert}
                setAlert={setAlert}
            />


        </>
    )
}
export default MainContent