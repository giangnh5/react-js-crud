import { Stack, Pagination, Container } from "@mui/material"

const PaginationTable = ({ noPage, page, setPage }) => {

    //Sự kiện đổi tranng thay đổi data
    const onChangePageClick = (event, value) => {
        setPage(value)
    }

    return (
        <>
            <Container>
                <Stack spacing={2} alignItems='end' marginTop={4} >
                    <Pagination count={noPage} defaultPage={page} onChange={onChangePageClick} shape="rounded" color='primary' />
                </Stack>
            </Container>

        </>
    )
}

export default PaginationTable