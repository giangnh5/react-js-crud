import { Container, Typography,Paper, Grid, Button,Table, TableHead, TableRow, TableCell, TableBody, TableContainer } from "@mui/material"

const Tables = ({arrTable, setOpenCreateModal, setOpenEditModal, setDataRow,setOpenDeleteModal,setDeleteId}) => {

    const onOpenCreatClick = () => {
        setOpenCreateModal(true);
    }

    const onDetailClick = (paramElement) => {
        setOpenEditModal(true); 
        setDataRow(paramElement);
    }

    const onDeleteClick = (paramElement) => {
        setOpenDeleteModal(true)
        setDeleteId(paramElement.id)
    }


    return (
        <>
            <Container maxWidth='xl'>
                <Typography variant='h4' textAlign='center' marginTop={3} > Thông tin khách hàng </Typography>
                <Grid container marginTop={3}>
                    <Grid item>
                        <Button style={{ marginInline: '10px' }} variant="contained" color='success' onClick={onOpenCreatClick}  > Thêm User </Button>
                    </Grid>
                </Grid>
                <Grid container marginTop={3}>
                    <Grid item xs={12} md={12} lg={12} sm={12}>
                        <TableContainer component={Paper}>
                            <Table sx={{ minWidth: 650 }} aria-label="simple table"  >
                                <TableHead>
                                    <TableRow >
                                        <TableCell align='center'>OrderId</TableCell>
                                        <TableCell align='center'>Tên</TableCell>
                                        <TableCell align='center'>Kích cỡ</TableCell>
                                        <TableCell align='center'>Loại Pizza</TableCell>
                                        <TableCell align='center'>Số điện thoại</TableCell>
                                        <TableCell align='center'>Trạng thái</TableCell>
                                        <TableCell align='center'>Action</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody >
                                    {
                                        arrTable.map((element, index) => {
                                            return (
                                                <TableRow key={index}  >
                                                    <TableCell align='center' >{element.orderId}</TableCell>
                                                    <TableCell align='center'>{element.hoTen}</TableCell>
                                                    <TableCell align='center'>{element.kichCo}</TableCell>
                                                    <TableCell align='center'>{element.loaiPizza}</TableCell>
                                                    <TableCell align='center'>{element.soDienThoai}</TableCell>
                                                    <TableCell align='center'>{element.trangThai}</TableCell>
                                                    <TableCell align='center'>
                                                        <Button style={{ marginInline: '10px' }} variant="contained" color='success' onClick={() => onDetailClick(element) } > Chi tiết </Button>
                                                        <Button style={{ width: '100px' }} variant="contained" color='error' onClick={() => onDeleteClick(element)} > Xóa </Button>
                                                    </TableCell>
                                                </TableRow>
                                            )
                                        })
                                    }

                                </TableBody>
                            </Table>
                        </TableContainer>
                    </Grid>
                </Grid>
            </Container>
        </>
    )
}
export default Tables