import { Grid, Button, Modal, Stack, TextField, InputLabel, Select, MenuItem, FormControl, Box, Typography } from "@mui/material"

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 15,
    p: 4
};


const EditModal = ({ openEditModal, setOpenEditModal, dataRow, setDataRow, arrDrink, setAlert }) => {

    const handleClose = () => {
        setOpenEditModal(false) 
    }

    // Lấy value Trạng thái
    const onStatusChange = (event) => {
        setDataRow({ ...dataRow, trangThai: event.target.value })
        console.log({ ...dataRow, trangThai: event.target.value })
    }

    // Xử lí Edit

    const handleEdit = () => {

        //B1 thu thập dữ liệu
        //B2 Validate - bỏ qua
        //B3 Call Api PUT
        fetchApi('http://42.115.221.44:8080/devcamp-pizza365/orders/' + dataRow.id, body)
            .then((data) => {
                setAlert({
                    status: true,
                    text: 'Edit thành công',
                    severity: 'success'
                })
                setOpenEditModal(false)
            })
            .catch((error) => {
                setAlert({
                    status: true,
                    text: 'Failed, Try again!',
                    severity: 'error'
                })
            })
    }

    const fetchApi = async (url, body) => {
        const response = await fetch(url, body);
        const data = await response.json();
        return data
    }

    let body = {
        method: 'PUT',
        body: JSON.stringify(dataRow),
        headers: {
            'Content-type': 'application/json; charset=UTF-8'
        }
    }

    return (
        <>
            <Modal
                open={openEditModal}
                onClose={handleClose}
            >
                <Box sx={style}>
                    <Typography variant="h5" textAlign='center' marginBottom={2}>Order Information</Typography>
                    <Grid container spacing={3} marginBottom={2}>

                        <Grid item xs={6} >
                            <Stack spacing={2}>
                                <FormControl fullWidth>
                                    <InputLabel id="demo-simple-select-label">Kích cỡ</InputLabel>
                                    <Select
                                        value={dataRow.kichCo}
                                        label="Kích cỡ"
                                        disabled
                                    >
                                        <MenuItem value='S'>Size S</MenuItem>
                                        <MenuItem value='M'>Size M</MenuItem>
                                        <MenuItem value='L'>Size L</MenuItem>
                                    </Select>
                                </FormControl>
                                <FormControl fullWidth>
                                    <InputLabel id="demo-simple-select-label">Loại nước uống</InputLabel>
                                    <Select
                                        value={dataRow.idLoaiNuocUong}
                                        label="Loại nước uống"
                                        disabled
                                    >
                                        {
                                            arrDrink.map((element, index) => {
                                                return (

                                                    <MenuItem value={element.maNuocUong} key={index}> {element.tenNuocUong} </MenuItem>
                                                )
                                            })
                                        }
                                    </Select>
                                </FormControl>
                                <FormControl fullWidth>
                                    <InputLabel id="demo-simple-select-label">Loại Pizza</InputLabel>
                                    <Select
                                        value={dataRow.loaiPizza}
                                        label="Loại Pizza"
                                        disabled
                                    >
                                        <MenuItem value='Seafood'>Hải Sản</MenuItem>
                                        <MenuItem value='Hawaii'>Hawaii</MenuItem>
                                        <MenuItem value='Bacon'>Thịt hun khói</MenuItem>
                                    </Select>
                                </FormControl>
                                <TextField label="Mã giảm giá" variant="outlined" disabled value={dataRow.idVourcher} />
                                <TextField label="Lời nhắn" variant="outlined" disabled value={dataRow.loiNhan} />
                            </Stack>

                        </Grid>
                        <Grid item xs={6} >
                            <Stack spacing={2}>
                                <TextField label="Đường kính" disabled variant="outlined" value={dataRow.duongKinh} />
                                <TextField label="Sườn" disabled variant="outlined" value={dataRow.suon} />
                                <TextField label="Salad" disabled variant="outlined" value={dataRow.salad} />
                                <TextField label="Số lượng nước" disabled variant="outlined" value={dataRow.soLuongNuoc} />
                                <TextField label="Thành tiền" disabled variant="outlined" value={dataRow.thanhTien} />
                            </Stack>
                        </Grid>
                    </Grid>
                    <hr />
                    <Grid container spacing={3} marginBottom={3} >
                        <Grid item xs={6}>
                            <Stack spacing={2}>
                                <TextField label="Họ và tên" variant="outlined" disabled value={dataRow.hoTen} />
                                <TextField label="Email" variant="outlined" disabled value={dataRow.email} />
                                <FormControl fullWidth>
                                    <InputLabel id="demo-simple-select-label">Trạng thái</InputLabel>
                                    <Select
                                        value={dataRow.trangThai}
                                        label="Trạng thái"
                                        onChange={onStatusChange}
                                    >
                                        <MenuItem value='open'>Open</MenuItem>
                                        <MenuItem value='cancel'>Đã hủy</MenuItem>
                                        <MenuItem value='confirmed'>Đã xác nhận</MenuItem>
                                    </Select>
                                </FormControl>

                            </Stack>
                        </Grid>
                        <Grid item xs={6}>
                            <Stack spacing={2}>
                                <TextField label="Số điện thoại" variant="outlined" disabled value={dataRow.soDienThoai} />
                                <TextField label="Địa chỉ" variant="outlined" disabled value={dataRow.diaChi} />
                            </Stack>
                        </Grid>
                    </Grid>



                    <Stack spacing={3}>
                        <Grid container textAlign='right'>
                            <Grid item xs={12} >
                                <Button variant="contained" color='error' onClick={handleClose} > Hủy bỏ </Button>
                                <Button variant="contained" color='success' style={{ marginInline: '10px', width: '100px'}} onClick={handleEdit}  > Edit </Button>
                            </Grid>
                        </Grid>
                    </Stack>

                </Box>
            </Modal>
        </>
    )
}

export default EditModal