import { Modal, Box, Typography, Grid, Button } from "@mui/material"

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 300,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4
};

const DeleteModal = ({ openDeleteModal, setOpenDeleteModal, deleteId, setAlert }) => {
    const handleClose = () => setOpenDeleteModal(false);
    // Xử lí sự kiện xóa
    const onDeleteClick = () => {
        fetchApi('http://42.115.221.44:8080/devcamp-pizza365/orders/' + deleteId, body)
            .then((response) => {
                setAlert({
                    status: true,
                    text: 'Xóa thành công',
                    severity: 'success'
                })
                setOpenDeleteModal(false)
            })
            .catch((error) => {
                setAlert({
                    status: true,
                    text: 'Failed, Try Again',
                    severity: 'error'
                })
            })
    }
    const fetchApi = async (url, body) => {
        const response = await fetch(url, body)
    }
    let body = {
        method: 'DELETE'
    };

    return (
        <>
            <Modal
                open={openDeleteModal}
                onClose={handleClose}
            >
                <Box sx={style}>
                    <Typography textAlign='center' variant="h6" >
                        Bạn có chắc chắn muốn xóa User này không?
                    </Typography>
                    <Grid container textAlign='center' marginTop={4}>
                        <Grid item xs={12} >
                            <Button variant="contained" color="error" style={{ marginInline: '10px' }} onClick={onDeleteClick}  > Xác nhận </Button>
                            <Button variant="contained" color="primary"  onClick={handleClose} > Hủy bỏ </Button>
                        </Grid>
                    </Grid>
                </Box>
            </Modal>
        </>
    )
}

export default DeleteModal