import { Grid, Button, Modal, Stack, TextField, InputLabel, Select, MenuItem, FormControl, Box, Typography } from "@mui/material"

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 15,
    p: 4
};

const CreateModal = ({ openCreateModal, setOpenCreateModal, arrDrink, objectCreate, setObjectCreate, setAlert }) => {

   
    const handleClose = () => {
        setOpenCreateModal(false)
    }

    const handleBtn = () => {
        //B1 thu thập dữ liệu
        //B2 Validate
        let validate = validateObject();
        if (validate) {
            // Call API Để POST dữ liệu
            fetchApi('http://42.115.221.44:8080/devcamp-pizza365/orders', body)
                .then((data) => {
                    setAlert({
                        status: true,
                        text: 'Thêm thành công',
                        severity: 'success'
                    })
                    setOpenCreateModal(false) 
                    setObjectCreate({
                        kichCo: '',
                        duongKinh: '',
                        suon: '',
                        salad: '',
                        loaiPizza: '',
                        idVourcher: '',
                        idLoaiNuocUong: '',
                        soLuongNuoc: '',
                        hoTen: '',
                        thanhTien: '',
                        email: '',
                        soDienThoai: '',
                        diaChi: '',
                        loiNhan: ''
                    })
                })
                .catch((error) => {
                    setAlert({
                        status: true,
                        text: 'Fail, Try again! ',
                        severity: 'error'
                    })
                })
        }
    }
    let body = {
        method: 'POST',
        body: JSON.stringify(objectCreate),
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
        },
    };

    // Thu thập thông tin
    const onKichCoChange = (event) => {
        const select = event.target.value
        setObjectCreate({ ...objectCreate, kichCo: select })
        if (select === 'S') {
            setObjectCreate({
                ...objectCreate,
                kichCo: "S",
                duongKinh: '20',
                suon: '2',
                salad: '200',
                soLuongNuoc: '2',
                thanhTien: 150000
            })
          
        }
        if (select === 'L') {
            setObjectCreate({
                ...objectCreate,
                kichCo: "L",
                duongKinh: '30',
                suon: '8',
                salad: '500',
                soLuongNuoc: '4',
                thanhTien: 250000
            })
            
        }
        if (select === 'M') {
            setObjectCreate({
                ...objectCreate,
                kichCo: "M",
                duongKinh: '25',
                suon: '4',
                salad: '300',
                soLuongNuoc: '3',
                thanhTien: 200000
            })
        }

    }
    // Loại nước uống
    const onLoaiNuocUongChange = (event) => {
        setObjectCreate({ ...objectCreate, idLoaiNuocUong: event.target.value })
    }
    // Loại pizza
    const onLoaiPizzaChange = (event) => {
        setObjectCreate({ ...objectCreate, loaiPizza: event.target.value })
    }
    // Voucher
    const onVoucherChange = (event) => {
        setObjectCreate({ ...objectCreate, idVourcher: event.target.value })
    }
    // Lời nhắn
    const onMessageChange = (event) => {
        setObjectCreate({ ...objectCreate, loiNhan: event.target.value })
    }
    // Họ và tên
    const onNameChange = (event) => {
        setObjectCreate({ ...objectCreate, hoTen: event.target.value })
    }
    // Số điện thoại
    const onPhoneChange = (event) => {
        setObjectCreate({ ...objectCreate, soDienThoai: event.target.value })
    }
    // Email
    const onEmailChange = (event) => {
        setObjectCreate({ ...objectCreate, email: event.target.value })
    }
    // Địa chỉ
    const onDressChange = (event) => {
        setObjectCreate({ ...objectCreate, diaChi: event.target.value })
        console.log({ ...objectCreate, diaChi: event.target.value })

    }

    const validateObject = () => {
        // Check phone
        let vnf_regex = /((09|03|07|08|05)+([0-9]{8})\b)/g;
        let checkPhone = RegExp(vnf_regex).test(objectCreate.soDienThoai)
        // Check Mail
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        let checkMail = RegExp(regex).test(objectCreate.email)

        if (objectCreate.kichCo === '') {
            setAlert({
                status: true,
                text: 'Size not Entered! ',
                severity: 'warning'
            })
            return false
        }
        if (objectCreate.idLoaiNuocUong === '') {
            setAlert({
                status: true,
                text: 'Quality of drink not Entered ',
                severity: 'warning'
            })
            return false
        }
        if (objectCreate.loaiPizza === '') {
            setAlert({
                status: true,
                text: 'Type of pizza not Entered ',
                severity: 'warning'
            })
            return false
        }
        if (objectCreate.hoTen === '') {
            setAlert({
                status: true,
                text: 'The name not Entered ',
                severity: 'warning'
            })
            return false
        }
        if (objectCreate.soDienThoai === '') {
            setAlert({
                status: true,
                text: 'The phone not Entered ',
                severity: 'warning'
            })
            return false
        }
        if (!checkPhone) {
            setAlert({
                status: true,
                text: 'Wrong format phone, Try again!',
                severity: 'error'
            })
            return false
        }
        if (objectCreate.email !== '' && !checkMail) {
            setAlert({
                status: true,
                text: 'Wrong format mail, Try again!',
                severity: 'error'
            })
            return false
        }
        if (objectCreate.diaChi === '') {
            setAlert({
                status: true,
                text: 'The Address not Entered',
                severity: 'error'
            })
            return false
        }
        if (objectCreate.idVourcher !== '') {
            callApiCheckVoucher();
        }
        return true
    }

    const fetchApi = async (url, body) => {
        const response = await fetch(url, body);
        const dataVoucher = await response.json();
        return dataVoucher
    }

    const callApiCheckVoucher = () => {
        fetchApi('http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/' + objectCreate.idVourcher)
            .then((data) => {
                console.log(data)
                setObjectCreate({ ...objectCreate, thanhTien: objectCreate.thanhTien - (objectCreate.thanhTien * data.phanTramGiamGia) / 100 })
            })
            .catch((error) => {
                console.log(error)
            })
    }

    return (
        <>
            <Modal
                open={openCreateModal}
                onClose={handleClose}
            >
                <Box sx={style}>
                    <Typography variant="h5" textAlign='center' marginBottom={2}>Thông Tin Order</Typography>
                    <Grid container spacing={3} marginBottom={2}>

                        <Grid item xs={6} >
                            <Stack spacing={2}>
                                <FormControl fullWidth>
                                    <InputLabel id="demo-simple-select-label">Kích cỡ</InputLabel>
                                    <Select
                                        value={objectCreate.kichCo}
                                        label="Kích cỡ"
                                        onChange={onKichCoChange}
                                    >
                                        <MenuItem value='S'>Size S</MenuItem>
                                        <MenuItem value='M'>Size M</MenuItem>
                                        <MenuItem value='L'>Size L</MenuItem>
                                    </Select>
                                </FormControl>
                                <FormControl fullWidth>
                                    <InputLabel id="demo-simple-select-label">Loại nước uống</InputLabel>
                                    <Select
                                        value={objectCreate.idLoaiNuocUong}
                                        label="Loại nước uống"
                                        onChange={onLoaiNuocUongChange}
                                    >
                                        {
                                            arrDrink.map((element, index) => {
                                                return (

                                                    <MenuItem value={element.maNuocUong} key={index}> {element.tenNuocUong} </MenuItem>
                                                )
                                            })
                                        }
                                    </Select>
                                </FormControl>
                                <FormControl fullWidth>
                                    <InputLabel id="demo-simple-select-label">Loại Pizza</InputLabel>
                                    <Select
                                        value={objectCreate.loaiPizza}
                                        label="Loại Pizza"
                                        onChange={onLoaiPizzaChange}
                                    >
                                        <MenuItem value='Seafood'>Hải Sản</MenuItem>
                                        <MenuItem value='Hawaii'>Hawaii</MenuItem>
                                        <MenuItem value='Bacon'>Thịt hun khói</MenuItem>
                                    </Select>
                                </FormControl>
                                <TextField label="Mã giảm giá" variant="outlined" onChange={onVoucherChange} value={objectCreate.idVourcher} />
                                <TextField label="Lời nhắn" variant="outlined" onChange={onMessageChange} value={objectCreate.loiNhan} />
                            </Stack>

                        </Grid>
                        <Grid item xs={6} >
                            <Stack spacing={2}>
                                <TextField label="Đường kính" disabled variant="outlined" value={objectCreate.duongKinh} />
                                <TextField label="Sườn" disabled variant="outlined" value={objectCreate.suon} />
                                <TextField label="Salad" disabled variant="outlined" value={objectCreate.salad} />
                                <TextField label="Số lượng nước" disabled variant="outlined" value={objectCreate.soLuongNuoc} />
                                <TextField label="Thành tiền" disabled variant="outlined" value={objectCreate.thanhTien} />
                            </Stack>
                        </Grid>
                    </Grid>
                    <hr />
                    <Grid container spacing={3} marginBottom={3} >
                        <Grid item xs={6}>
                            <Stack spacing={2}>
                                <TextField label="Họ và tên" variant="outlined" onChange={onNameChange} value={objectCreate.hoTen} />
                                <TextField label="Email" variant="outlined" onChange={onEmailChange} value={objectCreate.email} />
                            </Stack>
                        </Grid>
                        <Grid item xs={6}>
                            <Stack spacing={2}>
                                <TextField label="Số điện thoại" variant="outlined" onChange={onPhoneChange} value={objectCreate.soDienThoai} />
                                <TextField label="Địa chỉ" variant="outlined" onChange={onDressChange} value={objectCreate.diaChi} />
                            </Stack>
                        </Grid>
                    </Grid>



                    <Stack spacing={3}>
                        <Grid container textAlign='right'>
                            <Grid item xs={12} >
                                <Button variant="contained" color="success" style={{ margin: '10px' }} onClick={handleBtn}  > Thêm vào </Button>
                                <Button variant="contained" color="error" onClick={handleClose} > Hủy bỏ </Button>
                            </Grid>
                        </Grid>
                    </Stack>

                </Box>
            </Modal>
        </>
    )
}

export default CreateModal